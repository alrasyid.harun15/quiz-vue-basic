Vue.component('bio-list',{
	template:'#bioList',
	props:['gate'],
	data:function(){return{			
		}
	},
	methods:{
		edit:function(bio){
			console.log(`edit bio :${bio.nama} function called!`)
			this.$parent.createOrEdit = false
			this.$parent.bio = bio
			if(this.$parent.$refs.editBio){
				this.$parent.$refs.editBio.nama=bio.nama // set name after Edit Component mounted
			}
			
		},
		destroy:function(index){
			console.log(`delete at index : ${index} function called!`)
			if(confirm('Yakin hapus data?')){
				this.$parent.bios.splice(index,1)
			}
		},
	},
	mounted(){
		console.log('list component mounted!')				
	}
})

Vue.component('bio-create',{
	template:'#bioCreate',	
	data:function(){return{
			nama:''
		}
	},
	methods:{
		store:function(){

			if(!(this.nama==="")){
				this.$parent.bios.push(Bio(this.nama))
				alert('Simpan data berhasil!')
				this.nama=''
			}else{
				alert('Isi nama dahulu!')
			}			

		},
	},
	mounted(){
		console.log('create component mounted!')
	}
})

Vue.component('bio-edit',{
	template:'#bioEdit',
	props:['gate'],
	data:function(){return{
			nama:''
		}
	},
	methods:{
		update:function(){
			console.log(`Update bio :${this.nama} function called!`)
			this.gate.nama = this.nama
			this.$parent.createOrEdit = true			
			alert('Ubah data berhasil!')
		},
	},
	mounted(){
		console.log('edit component mounted!')
		this.nama = this.gate.nama
	}
})

var app = new Vue({
  el: '#vueApp',
  data: {
	message: 'Quiz Sanbercode Vue Basic!',
	bios:[],
	createOrEdit:true, //true means create, false means edit
	bio:'' //edit Passing for mounted
  },
  mounted(){
	  this.bios = [
			Bio('Muhammad Iqbal Mubaroq'), 
			Bio('Ruby Purwanti'),
			Bio('Faqih Muhammad'),
		]
  }
  
})

function Bio(nama){
	return{
		nama:nama
	}
}